// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title:
          RichText(
            text: TextSpan(
              children: [
                TextSpan(text: 'Картинка и меню ', style: TextStyle(color: Colors.white)),
                WidgetSpan(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 2.0),
                    child: Icon(Icons.image),
                  ),
                ),
              ],
            ),
          ),
          actions: [
            Icon(Icons.add),
          ],
          leading: Icon(Icons.arrow_back),
        ),
        body: Column(
          children: <Widget>[
            Expanded(child: Align(
              alignment: Alignment.centerLeft,
              child: Icon(Icons.add,
                  size: 50,
                  color: Colors.blueGrey),
            )),
            Expanded(
                flex:2,
            child: Stack(
              children: [
                Container(
                  decoration: const BoxDecoration(
                  image: DecorationImage(
                  image: AssetImage('img/background.jpg'),
                  fit: BoxFit.fill),
                  ),
                ),
                Center(
                  child: Container(
                    width: 150,
                    height: 150,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      color: Colors.blueGrey,),
                    child: Text('Действие', style: TextStyle(fontSize: 20, color: Colors.white)),
                    alignment: Alignment.center,
                  ),
                ),
              ],)
            ),
            Expanded(child: Align(
              alignment: Alignment.centerRight,
              child: Icon(Icons.delete,
                  size: 50,
                  color: Colors.blueGrey),
            )),
          ],
        ),
      ),
    );
  }
}